FROM alpine:3.18

RUN apk --no-cache add \
    bash~=5.2 \
    openssh~=9.3 \
    curl~=8.5 && \
    curl -fsSL -o /common.sh https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
