# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.8.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.8.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.7.1

- patch: Update documentation: Use base64 encoded values for secured variables if passing them as part of ENV_VARS variable to prevent exposure in the logs.

## 0.7.0

- minor: Update alpine docker image and packages in Dockerfile.

## 0.6.1

- patch: Update a link to the guide for multiple SSH keys usage.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 0.4.3

- patch: Internal maintenance: bump package versions for testing.
- patch: Internal maintenance: bump pipes versions in pipelines config file.
- patch: Refactor to use bash arrays to prevent quoting / word spliting issues.

## 0.4.2

- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the pipe's dependencies.
- patch: Internal maintenance: Update the README.

## 0.4.1

- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update dependencies in test/requirements.
- patch: Internal maintenance: update tests to provide more coverage.
- patch: Internal maintenance: update the version of the bitbucket-pipe-release pipe.

## 0.4.0

- minor: Updated information about DEBUG attribute.

## 0.3.1

- patch: Update README: add EXTRA_ARGS usage examples.

## 0.3.0

- minor: Add support for ENV_VARS variable.

## 0.2.8

- patch: Fix support for EXTRA_ARGS variable in the command mode.

## 0.2.7

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.2.6

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.2.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.2.4

- patch: Update the Readme with examples passing local environment variables to the remote host.

## 0.2.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.2

- patch: Updated readme about alternative base64 encoded SSH_KEY

## 0.2.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 0.2.0

- minor: Update pipes toolkit version to avoid capturing the output into a variable when using run command. This prevents issues with large /dev/stdout output.

## 0.1.4

- patch: Documentation updates

## 0.1.3

- patch: Fixed the default value for MODE parameter

## 0.1.2

- patch: Update the yaml definition

## 0.1.1

- patch: Updated contributing guidelines

## 0.1.0

- minor: Initial release
